//task1

function FizzBuzz(arg)
{
    if(arg%3==0 && arg%5==0)
    {
        console.log("FizzBuzz ,");
    }
    else if(arg%3==0)
    {
        console.log("Fizz ,");
    }
    else if(arg%5==0)
    {
        console.log("Buzz ,");
    }
    else 
    {
        console.log(arg+" ,");
    }
}

let i=1;
do{
    FizzBuzz(i);
    i++;
}while(i<100);

//task 2.1

let arr=[[1,2,3],23,'hello',[6,5,6],8,'ok'];
function array(a){
    let b=[];
    let z=0;
    for(let k in a)
    {
        if(Array.isArray(a[k]))
        {
            b[z]=a[k];
            z++;
        }
    }
    return b;
}

console.log(array(arr));

//task 2.2

function array2(a){
    let b=[];
    let k=0;
    for(let key of a)
    {
        if(Array.isArray(key))
        {
            for(let key2 of key)
            {
                let p=true;
                for(let i=0;i<b.length;i++)
                {
                    if(b[i]==key2)
                    {
                        p=false;
                        break;
                    }
                }
                if(p){
                    b[k]=key2;
                    k++;
                }
            }
        }
    }
    return b;
}

console.log(array2(arr).sort());
